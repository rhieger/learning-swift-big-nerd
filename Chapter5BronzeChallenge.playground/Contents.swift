//: Playground - Chapter 5 Bronze Challenge: page 121

import Cocoa

// NOTE:
//
// The challenge is to first evaluate the code printed on page 121
// and determine its output. Then the result should be checked by
// entering the code in a playground.

// MY GUESS:
//
// Output would be:
//
// x: 1, y:4 is in quadrant 1.

// NOW LET'S TEST MY THEORY.

let point = (x: 4, y: 1)

switch point    {
    
case let q1 where (point.x > 0) && (point.y) > 0:
    print("\(q1) is in quadrant 1.")
    
case let q2 where (point.x < 0) && (point.y) > 0:
    print("\(q2) is in quadrant 2.")
    
case let q3 where (point.x < 0) && (point.y < 0):
    print("\(q3) is in quadrant 3.")
    
case let q4 where (point.x > 0) && (point.y < 0):
    print("\(q4) is in quadrant 4.")
    
case (_, 0):
    print("\(point) rests on the x-axis.")
    
case (0, _):
    print("\(point) rests on the y-axis.")
    
default:
    print("Case not covered.")
    
}   // end switch point


// RESULT:
//
// Well, I was mostly correct. However, the style of output did not
// include the designators for the values. Further, as the values
// are in a tuple, they are enclosed in parentheses in the output.
// I can reset assured, however, that my logic was correct.