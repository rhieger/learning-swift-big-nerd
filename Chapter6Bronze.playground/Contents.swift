//: Playground - Chapter 6 Bronze Challenge

import Cocoa

// Bronze Challenge: page 145:
//
// Use a loop to count by 2 from 0 up to 100. Use another loop
// to make sure the first loop is run 5 times.
//
// Hint: one good way to do this is to use a nested loop.

// MY SOLUTION:

var counter = 0                         // Value of counter

var iterationCount = 0                  // Value of iteration count

while iterationCount < 5    {

    for counter in 0...100  {
        
        if counter == 0 {
        
            print("Counter = \(counter).")
            
            // Now test
            
        }   else if counter % 2 == 0   {
            
            print("Counter = \(counter).")
            
        }   // end if-else
        
    }   // end for _ in counter 0...100
    
    // Iterate iterationCount.
    
    iterationCount += 1
    
    // Print blank line to console.
    
    print()

}   // end while
