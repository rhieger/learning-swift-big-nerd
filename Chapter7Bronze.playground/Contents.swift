//: Playground - Chapter 7 Bronze Challenge

import Cocoa

// Bronze Challenge, page 161:
//
// Replace the "Hello" string with an instance created out of its
// corresponding Unicode scalars. You can find the appropriate codes
// on the Internet.

// MY SOLUTION:

var capHScalar = "\u{0048}"                // H
var eScalar =    "\u{0065}"                // e
var lScalar =    "\u{006c}"                // l
var oScalar =    "\u{006f}"                // o

let compositeScalar = "\(capHScalar)\(eScalar)\(lScalar)\(lScalar)\(oScalar)"

// Log result to console.

print(compositeScalar)
