//: Playground - Conditionals: Helping Applications Make Decisions

import Cocoa

// Decide weather a town is big or small depending on its population.

var population: Int = 5422

var message: String

if population < 10000   {
    
    message = "\(population) is a small town!"
    
    }   else if population >= 10000 && population < 50000    {
    
        message = "\(population) is a medium town!"
        
    }   else if population >= 100000 && population < 1000000 {
    
        // NOTE: This else-if block solves the Bronze Challenge
        // for Chapter 3:
        //
        // Add an additional else if statement to the town-sizing code
        // to see if your town’s population is very large. Choose your
        // own population thresholds. Set the message variable
        // accordingly.
    
        message = "\(population) is a very large town!"
    
    }   else    {
        
        message = "\(population) is pretty big!"
        
    }   // end if-else

// NOTE: The code below, when uncommented replaces the conditional
// logic of the above if-else block with a ternary operator. Of
// course, in order to use this, the code above must be commented
// out.

//message = population < 10000 ? "\(population) is a small town!"
//    : "\(population) is pretty big!"

// Log output to console.

print(message)

// Check if the town has a post office.

var hasPostOffice: Bool = true              // Boolean true: there is
                                            // a post office in the town.

if !hasPostOffice  {
    
    print("\nWhere do we buy stamps?")
    
}   // end if
