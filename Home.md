# Exercises and Challenges from **_Swift Programming—The Big Nerd Ranch Guide_**

## Table of Contents

1. Getting Started
2. Types, Constants, and Variables
3. Conditionals
4. Numbers
5. Switch
6. Loops
7. Strings
8. Optionals
9. Arrays
10. Dictionaries
11. Sets
12. Functions
13. Closures
14. Enumerations
15. Structs and Classes
16. Properties
17. Initialization
18. Value vs. Reference Types
19. Protocols
20. Error Handling
21. Extensions
22. Generics
23. Protocol Extensions
24. Memory Management and ARC
25. Equatable and Comparable
26. Your First Cocoa Application
27. Your First iOS Application
28. Interoperability
29. Conclusion

***

# Resources #

***

[Swift Comparison Operators](https://bitbucket.org/rhieger/learning-swift-big-nerd/wiki/Swift%20Comparison%20Operators)  
[Swift Logical Operators](https://bitbucket.org/rhieger/learning-swift-big-nerd/wiki/Swift%20Logical%20Operators)  
[The Remainder Operator—%, Apple Developer Documentation](https://developer.apple.com/library/ios/documentation/Swift/Conceptual/Swift_Programming_Language/BasicOperators.html)  
[Wikipedia Article: Significand—Referred to as Mantissa in Computer Science](https://bitbucket.org/rhieger/learning-swift-big-nerd/downloads/Significand.pdf)  
[Electronics Tutorials: Signed Binary Numbers](https://bitbucket.org/rhieger/learning-swift-big-nerd/downloads/Signed%20Binary%20Numbers%20and%20Two's%20Complement%20used%20in%20Binary.pdf)  
[Signed Numbers in Binary - Andy Bargh](https://bitbucket.org/rhieger/learning-swift-big-nerd/downloads/Signed%20Numbers%20in%20Binary%20-%20AndyBargh.pdf)  
[Wikipedia Article: List of Unicode Characters](https://bitbucket.org/rhieger/learning-swift-big-nerd/downloads/list-unicode-characters-wikipedia.pdf)