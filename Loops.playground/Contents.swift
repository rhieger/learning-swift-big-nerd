//: Playground - Chapter 6 Loops

import Cocoa

// First declare Interger variable.

var myFirstInt: Int = 0                        // To be used in for-in loop

//for i in 1...5  {
//    
//    // Increment myFirstInt.
//    
//    myFirstInt += 1

    // NOTE: This code differs from that in the book, which
    // uses the ++ increment operator, now deprecated in
    // Swift 2.2. In its place is the += addition assignment
    // operator which achieves the same result.
    
    // Log results to console.
    
//    print(myFirstInt)
//    
//}   // end for-in



// NOTE:
//
// The above code is commented out in favor of the refactored
// version of the code below that logs the changing value of
// constant i to the console.

//for i in 1...5  {

    // Increment myFirstInt.
    
//    myFirstInt += 1

    // Log result, showing changing value of constant i,
    // to the console.
    
//    print("myFirstInt equals \(myFirstInt) at iteration \(i).")

//}    end for i in 1...5



// NOTE:
//
// The above code is commented out in favor of the refactored
// version of the code below that replaces the constant i with
// a wildcard _. This is done typically when you do not need to
// reference the count of the iterator (i) within the loop.

//for _ in 1...5  {
//    
//    // Increment myFirstInt.
//    
//    myFirstInt += 1
//    // Log result to the console.
//    
//    print(myFirstInt)
//    
//}   // end for _ in 1...5


// NOTE: The above for _ in loop is commented out in favor of
// the following refactored traditional for loop, which accomplishes
// the same task.

for var i = 1; i < 6; i += 1    {
    
    // First increment myFirstInt.
    
    myFirstInt += 1
    
    // Now log the result to the console.
    
    print(myFirstInt)
    
}   // end for

// NOTE:
//
// There are two important things to note in the above
// refactored code.
//
// 1. Rather than using the increment ++ operator both
//    in the outer traditional for loop and in the
//    internal increment statement the addtion assignment
//    (+=) is used. The increment operator is deprecated.
//
// 2. Even more importantly, the traditional for loop as
//    implement above is also deprecated and will be removed
//    from future versions of Swift (probably the near future).

// A for-in loop with a case clause:
//
// The following loop will log only those integers that are
// multiples of 3 to the console.

// First, let's skip a line in the console.

print()

for case let i in 1...100 where i % 3 == 0     {
    
    print("\(i) is a multiple of 3.")
    
}   // end for case let i in 1...100 where i % 3 == 0


// Above classical for loop refactored as a while loop.

var i = 1                                   // Counter variable

// Reset value of myFirstInt to 0.

myFirstInt = 0

// Log blank line to console.

print()

while i < 6     {
    
    // Increment myFirstInt
    
    myFirstInt += 1
    
    // Log values to console.
    
    print(myFirstInt)
    
    
    // Increment counter i
    
    i += 1
    
    // NOTE: As in previous cases the above code uses the addition
    //       assignment operator (+=) in lieu of the increment ++
    //       operator as this is deprecated and due to be removed
    //       from the Swift language specification.
    
}   // end while


// NOTE: The above implementation of while loop is not a typical use
//       case. Typically, a while loop is used when one does not know
//       how many iterations will occur. Thus here is a completely
//       dumbed-down example involving a game of battle in space.
//       Based on the incomplete example in the book, the ship fires
//       blasters at an attacking ship until its shields give way.

var shieldStrength: Int = 100               // 100% shield strength

// Log blank line to console.

print()

while shieldStrength >= 0    {
    
    // Fire blasters.
    
    print("Fire blasters!")
    
    // Log message about shield strength.
    
    if shieldStrength == 0  {
        
        print("Shields have totally collapsed!\n")
        
    }   else    {
        
        print("Shield strength at \(shieldStrength)%!\n")
        
    }   // end if-else
    
    // Decrement shieldStrength
    
    shieldStrength -= 1
    
}   // end while


// Refactor the above code to use a repeat-while loop:

// Insert blank line on console.

print()

// Reset shieldStrength to 100%.

shieldStrength = 100

repeat {
    
    // Fire blasters.
    
    print("Fire blasters!")
    
    // Log message about shield strength.
    
    if shieldStrength == 0  {
        
        print("Shields have totally collapsed!\n")
        
    }   else    {
        
        print("Shield strength at \(shieldStrength)%!\n")
        
    }   // end if-else
    
    // Decrement shieldStrength
    
    shieldStrength -= 1
    
}   while shieldStrength >= 0


// Use continue statement to transfer control back to beginning of
// while loop on a given condition. This code refactors and changes
// the functionality of the repeat-while loop above.

//var shields = 5                               // Shield strength value
//
//var blastersOverheating = false              // Blasters not overheated
//var blasterFireCount = 0                     // Number of times blasters fired
//
//while shields > 0   {
//    
//    // Test whether blasters have overheated.
//    
//    if blastersOverheating  {
//        
//        print("\nBlasters are overheated! Cooldown initiated.")
//        
//        sleep(5)                            // Pause for 5 seconds
//        
//        print("Blasters ready to fire.")
//        
//        sleep(1)                            // Pause for 1 second
//        
//        // Reset blasters.
//        
//        blastersOverheating = false
//        
//        // Reset blasterFireCount.
//        
//        blasterFireCount = 0
//        
//    }   // end if
//    
//    // Check number of times blasters have been fired.
//    
//    if blasterFireCount > 100   {
//        
//        // Set overheating to true.
//        
//        blastersOverheating = true
//        
//        continue        // Return control to beginning of while loop.
//        
//    }   // end if
//    
//    // Log blaster fire to console.
//    
//    print("\nFire blasters!")
//    
//    // Increment blaster fire count.
//    
//    blasterFireCount += 1
//    
//}   // end while



// NOTE: The above code is commented out in favor of the refactored while
//       loop below, which fixes the infinite loop created in the above
//       code. To do this a break statement is introduced.

var shields = 5                              // Shield strength value

var blastersOverheating = false              // Blasters not overheating

var blastersFireCount = 0                    // Number of times blasters fired

// NOTE: Game over when 500 space demons are destroyed.

var spaceDemonsDestroyed = 0                // Number of space demons destroyed

while shields > 0   {
    
    if spaceDemonsDestroyed == 500  {
        
        print("\nYou beat the game!")
        
        break                               // End program
        
    }   // end if
    
    
    if blastersOverheating  {
        
        print("\nBlasters overheated! Initiating cooldown.")
        
        sleep(5)                            // Pause for 5 seconds
        
        print("Blasters ready to fire.")
        
        sleep(1)                            // Pause for 1 second
        
        // Reset blasters.
        
        blastersOverheating = false
        
        // Reset blastersFireCount.
        
        blastersFireCount = 0
        
        // Pass control to beginning of while loop.
        
        continue
        
    }   // end if
    
    if blastersFireCount > 100  {
        
        // Set overheating to true.
        
        blastersOverheating = true
        
        // Pass control to beginning of while loop.
        
        continue
        
    }   // end if
    
    // Fire blasters.
    
    print("\nFire blasters!")
    
    // Increment blastersFireCount.
    
    blastersFireCount += 1
    
    // Increment spaceDemonsDestroyed.
    
    spaceDemonsDestroyed += 1
    
}   // end while
