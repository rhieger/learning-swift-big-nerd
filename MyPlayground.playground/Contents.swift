//: MyPlayground - first baby steps in Swift. Starting on
// page 36, Chapter 1.

import Cocoa

// Create a variable of type String.

var str = "Hello, playground"

// Using the addition assignment operator, concatenate an
// exclamation point to the end of the variable str.

str += "!"

// Log output to console.

print(str)

// BRONZE CHALLENGE, Page 48:
//
// Many of the chapters in this book end with one or more challenges. The
// challenges are for you to work through on your own to deepen your
// understanding of Swift and get a little extra experience. Your first
// challenge is below. Before you get started, create a new playground.
//
// You learned about the String type and printing to the console using
// print(). Use your new playground to create a new instance of the
// String type. Set the value of this instance to be equal to your
// last name. Print its value to the console.

var lastName: String = "Hieger"

print("\nMy last name is \(lastName).")