//: Playground - Numbers: This playground explores number types.

import Cocoa

// Get maximum and minimum values of Int data type:

print("The maximum value for the Int type is \(Int.max).\n")

print("The minimum value for the Int type is \(Int.min).\n")

// Get maximum and minimum values for Int32 data types:

print("The maximum value for the Int32 type is \(Int32.max).\n")

print("The minimum value for the Int32 type is \(Int32.min).\n")

// Get maximum and minimum values for unsigned integer types:

print("The maximum UInt value is \(UInt.max).\n")

print("The minimum UInt value is \(UInt.min).\n")

print("The maximum UInt32 value is \(UInt32.max).\n")

print("The minimum UInt32 value is \(UInt32.min).\n")

// Explicit and implicit declaration of Integers:

// Explicit Declaration of Int Type:

let numberOfPages: Int = 10

// Implicit Declaration of Int Type; type inferred by
// Swift compiler:

let numberOfChapters = 3

// Log results to console.

print("numberOfPages = \(numberOfPages).\n")

print("numberOfChapters = \(numberOfChapters).\n")

// Explicit Declaration of Other Int Types:

// Declare UInt (Unsigned Integer) Explicitly:

let numberOfPeople: UInt = 40

let volumeAdjustment: Int32 = -1000

// Log results to console:

print("numberOfPeople = \(numberOfPeople).\n")

print("volumeAdjustment = \(volumeAdjustment).\n")

// Declaring Integer Types With Invalid Values:

// Declare a UInt with a negative value--Error!

//let firstBadValue: UInt = -1

// Declare an Int8 with a value greater than 127--Error!

//let secondBadValue: Int8 = 200

// NOTE: The above two values are commented out because they introduce
// both overflow and underflow errors to the playground.

// Perform basic mathematical operations:

print("10 + 20 = \(10 + 20).\n")    // Addition
print("30 - 5 = \(30 - 5).\n")      // Subtraction
print("5 * 6 = \(5 * 6).\n")        // Multiplication

// Operator Precedence

// NOTE: This basically follows the rules of the nemonic scheme PEMDAS
// (Please Exuse My Dear Aunt Sally), which corresponds to the order in
// which operators are used:
//
// 1. Parenthesis (anything within parentheses)
// 2. Exponents (powers of anything)
// 3. Multiplication
// 4. Division
// 5. Addition
// 6. Subtraction

// 20, because 2 * 5 is evaluated first:

print("10 + 2 * 5 = \(10 + 2 * 5).\n")

// 20, because 30 - 5 is evaluated first:

print("30 - 5 - 5 = \(30 - 5 - 5).\n")

// Simplifying operator precedence with parenthesis:
//
// NOTE: Since parentheses are always processed first, an expedient
// is to use parentheses to clarify the meaning of your operations
// as follows.

// Equal to 60 because (10 + 2) is evaluated first; thus 12 * 5 = 60.

print("( (10 + 2) * 5) = \( (10 + 2) * 5 ).\n")

// Equal to 30 because (5 - 5) is evaluated first; thus 30 - 0 = 30.

print("( 30 - (5 - 5) = \( 30 - (5 - 5) ).\n")

// Unexpected Results from integer division:

// NOTE: Integer Division only returns the whole number portion
// of the number, if there is a remainder.

print("11 / 3 = \(11 / 3).\n")

// The Remainder Operator (% Operator):

print("11 % 3 = \(11 % 3).\n")      // Logs a remainder (modulus) of 2
print("-11 % 3 = \(-11 % 3).\n")    // Logs a remainder (modulus) of -2

// Incrementing and Decrementing

// NOTE: The book makes use of the ++ (increment) and -- (decrement)
// operators, which have been officially deprecated as of Swift 2.2.
// The same function should now be achieved with the += (addition
// assignment) and -= (subtraction assignment) operators instead, as
// seen below.

var x = 10

// Log to console:

print("x = \(x).\n")

// Increment x using addition assignment operator:

x += 1

// Log result to console:

print("x has been incremented to \(x).\n")

// Decrement x using subtraction assignment operator:

x -= 1

// Log result to console:

print("x has been decremented to \(x).\n")

// Using Addition Assignment Operator with Value Other Than 1:

x += 10

// Log result to console:

print("x has had 10 added to it and is now \(x).\n")    // Outputs 20

// Causing an overflow error:

let y: Int8 = 120                   // NOTE: Int8 has minimum value of -128
                                    // and maximum value of 127.

//let z = y + 10

// NOTE: The above assignment operator results in a value of 130 being
// assigned to the Int8 Type designated as z. As Int8 has maximum value
// of 127, this causes a trap error. Therefore the line is commented out.

// Using an Overflow Operator:
//
// This section demonstrates the use of an overload operator that handles
// the overflow by wrapping back to values allowed by the data type. Though
// the default behavior in most C-influenced languages, Swift chooses not
// to provide this behavior by default, emphasizing strict avoidance of
// overflows or underflows. However, for rare instances, the following
// syntax is available:

let z = y &+ 10

// Log to console.

print("120 &+ 10 = \(z).\n")

// Adding Values of Different Types.

// Values of different Types must be converted so that they are all the
// same type; otherwise, a compile time error results.

let a: Int16 = 200

let b: Int8 = 50

// Now add them together.

//let c = a + b   // Program crashes!

// NOTE: Above line commented out to prevent program from crashing.

// Converting Type to Allow Addition of Unlike Type:
//
// NOTE: Technically, either a could be converted to Int8 type, or
// b can be converted Int16 type. But only one will work. If a is
// converted to Int8, we have another problem, because its value,
// 200, is larger than the maximum value for Int8, 127. Therefore,
// b must be converted to Int16.

let c = a + Int16(b)

// Log result to console.

print("\(a) + \(b) = \(c).\n")

// Explicit Declaration of Floating Point Types

let d1 = 1.1                    // Implicitly Double (Inferred Type)

let d2: Double = 1.1            // Explicitly Double (Declared Type)

let f1: Float = 100.3           // Explicitly Float (Declared Type)

// Log Results:

print("d1 is an implicit Double whose value is: \(d1)\n")

print("d2 is an explicit Double whose value is: \(d2)\n")

print("f1 is an explicit Float whose value is: \(f1)\n")

// Floating Point Number Operations

// Log to console examples of floating point operations:

print("10.0 + 11.4 = \(10.0 + 11.4)\n")

print("11.0 / 3.0 = \(11.0 / 3.0)\n")

print("12.4 % 5.0 = \(12.4 % 5.0)\n")

// Comparing Two Floating Point Numbers:

// Refresh Our Memory for d1 and d2 Values

print("The value of d1 is: \(d1).\n")

print("The value of d2 is: \(d2).\n")

// Now let's compare them to reveal the imprecision.

print("Are d1 and d2 really equal?\n")

// Check for equality:

if d1 == d2     {
    
    print("d1 and d2 are equal.\n")
    
}   // end if

// So far, so good. Now let's add to d1.

print("d1 + 0.1 = \(d1 + 0.1)\n")

// But is d1 + 0.1 equal to 1.2? Let's find out.

if d1 + 0.1 == 1.2  {
    
    print("d1 + 0.1 = 1.2\n")
    
}   else    {
    
    print("No! Actually, d1 + 0.1 is not equal to 1.2!\n")
    
}   // end if-else
