//: Playground - Optionals: Chapter 8

import Cocoa

// Declare an optional type.

var errorCodeString: String?

// Initialize errorCodeString.

errorCodeString = "404"

// Log value of optional to console.

print(errorCodeString)

// Log nil value of optional to console.

// 1. Declare a nil optional

var errorCodeString1: String?

// 2. Log nil to console.

print("\n\(errorCodeString1)")


// Add a condition to check if optional is a nil value;
// if so, log error message to console.


//if errorCodeString != nil   {
//    
//    let theError = errorCodeString!
//    
//    // NOTE: Exclamation point at end of errorCodeString
//    //       denotes forcible unwrapping of the optional.
//    
//    // Log value to console.
//    
//    print("\nEncountered Error: \(theError)")
//    
//}   // end if

// Conditional Binding of Optional to Value:

// The above conditional is commented out in favor of the refactored
// optional below that makes use of conditional binding. This produces
// a temporary constant in the first clause of the conditional
// statement which is used if and only if a value is passed from the
// constant errorCodeString. If errorCodeString is nil, the first
// clause of the conditional does not run.

if let theError = errorCodeString   {
    
    print("\nEncountered Error: \(theError)")
    
}   // end if


// Nesting Optional Bindings:
//
// The nested optional binding below converts the String type
// theError to an Integer Type.

if let theError = errorCodeString   {
    
    if let errorCodeInteger = Int(theError)  {
        
        print("\nEncountered Error: \(theError): \(errorCodeInteger)")
        
    }   // end if let
    
}   // end if let
