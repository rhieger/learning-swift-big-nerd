//: Playground - Strings: Chapter 7

import Cocoa

let playground = "Hello, playground."

// Create a mutable version of playground string.

var mutablePlayground = "Hello, mutable playground"

// Concatenate ! to mutablePlayground.

mutablePlayground += "!"

// Looping through characters in a String:

for c: Character in mutablePlayground.characters    {
    
    print("'\(c)'")
    
}   // end for

// Declare and log a unicode scalar constant.

let oneCoolDude = "\u{1f60e}"

print("\n\(oneCoolDude)")

// Declare a combined unicode scalar contstant and log it to console.

let aAcute = "\u{0061}\u{0301}"

print("\n\(aAcute)")

// Display all the unicode sclars that comprise the String
// constant playground.

// Print blank line to console.

print()

for scalar in playground.unicodeScalars {
    
    print("\(scalar) = \(scalar.value)")
    
}   // end for-in


// Using a precomposed unicode scalar.
//
// NOTE: The code below produces an identical result to the constant
//       aAcute declared above, but does not need to composite two
//       separate scalars to produce one character.

let aAcutePrecomposed = "\u{00e1}"

// Log result to console.

print("\nUnicode 00e1 = \(aAcutePrecomposed).")

// Checking for equivalence.
//
// NOTE: Constants aAcute and aAcutePrecomposed are technically not
//       equivalent. aAcute is comprised of two Unicode scalars,
//       whereas aAcutePrecomposed is one pre-composed Unicode scalar.
//       Swift will treat them as equivalent because they are
//       canonically equivalent, that is, they both mean the same
//       thing and are linguistically equivalent.

let b = (aAcute == aAcutePrecomposed)   // true

// Log to console.

print("\naAcute == aAcutePrecomposed: \(b)")

// Checking Character Count for Canonically Equivalent Unicode Scalars:

print("\naAcute: \(aAcute.characters.count) character;",
      "aAcutePrecomposed: \(aAcutePrecomposed.characters.count) character")

// NOTE:
//
// Even the number of characters is considered to be the same by Swift,
// because of canonical equivalence. This is despite the fact that aAcute
// is comprised of two Unicode scalars.

// Find the Fifth Character in String Constant playground:

let fromStart = playground.startIndex            // Starting index of playground

let toPosition = 4                              // Index of fifth character

let end = fromStart.advancedBy(toPosition)      // Desired index

let fifthCharacter = playground[end]            // Value of fifth character

// Log result to console.

print("\nThe fifth character in constant playground: \(fifthCharacter)")

// Extracting a range of characters from playground:

// 1. Set the range of characters.

let range = fromStart...end

// 2. Extract first five characters.

let firstFive = playground[range]           // Extracts "Hello"

// 3. Log result to console.

print("\nfirstFive = \(firstFive)")
