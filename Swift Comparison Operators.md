# Comparison Operators #

Operator | Description
-------- | -----------
**<**    | Evaluates whether the number on the left is smaller than the number on the right.
**<=**   | Evaluates whether the number on the left is smaller than or equal to the number on the right.
**>**    | Evaluates whether the number on the left is greater than the number on the right.
**>=**   | Evaluates whether the number on the left is greater than or equal to the number on the right.
**==**   | Evaluates whether the number on the left is equal to the number on the right.
**!=**   | Evaluates whether the number on the left is not equal to the number on the right.
**===**  | Evaluates whether the two instances point to the same reference.
**!==**  | Evaluates whether the two instances do not point to the same reference.

**NOTE:** This table is taken from **Table 3.1 Comparison Operators**, Chapter 3.

[Wiki Home Page](https://bitbucket.org/rhieger/learning-swift-big-nerd/wiki/Home)