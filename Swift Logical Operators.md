# Swift Logical Operators #

Operator | Description
-------- | -----------
**&&**   | Logical **AND**: true if an only if both are true (false otherwise)
**11**   | Logical **OR**: true if either is true (false only if both are false)
**!**    | Logical **NOT**: true becomes false, false becomes true

**NOTES:**

1. This table derived from **Table 3.2: Logical Operators**, **_Swift Programming: The Big Nerd Ranch Guide_**, Chapter 3.
2. Note that logical OR is incorrectly represented as two 1's. This is a limitation of Markdown. The proper symbol is comprised of two vertical bars, like this: **||**.

[Wiki Home Page](https://bitbucket.org/rhieger/learning-swift-big-nerd/wiki/Home)