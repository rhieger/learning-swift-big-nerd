//: Playground - Switch: Chapter 5

import Cocoa

//var statusCode: Int = 404
//
//var errorString: String                             // Message for error code
//
//switch statusCode   {
//    
//case 400:
//    errorString = "Bad request"
//    
//case 401:
//    errorString = "Unauthorized"
//    
//case 403:
//    errorString = "Forbidden"
//    
//case 404:
//    errorString = "Not found"
//    
//default:
//    errorString = "None"
//    
//}   // end switch statusCode


// NOTE:
//
// Above code commented out in favor of second version of switch code
// block. This new version uses a single switch case with multiple values.

//var statusCode: Int = 404
//
//var errorString: String = "The request failed with the error: "
//
//switch statusCode {
//
//case 400, 401, 403, 404:
//    errorString = "There was something with the request. "
//    fallthrough
//    
//default:
//    errorString += "Please review the request and try again."
//
//}   // end switch statusCode


// NOTE:
//
// Above code commented out in favor of third version of switch code
// block. This new version demonstrates a mix of switch cases with
// one value, more than one value, and a range of values.

//var statusCode: Int = 404
//
//var errorString: String = "The request failed with the error: "
//
//switch statusCode   {
//    
//case 100, 101:
//    errorString += " Informational, 1xx."
//    print(errorString)
//    
//case 204:
//    errorString += " Successful, but no content, 204"
//    print(errorString)
//    
//case 300...307:
//    errorString += " Redirection, 3xx."
//    print(errorString)
//    
//case 400...417:
//    errorString += " Client error, 4xx."
//    print(errorString)
//    
//case 500...505:
//    errorString += " Server error, 5xx."
//    print(errorString)
//
//default:
//    errorString += "Unknown. Please review the request and try again."
//    print(errorString)
//    
//}   // end switch statusCode


// NOTE:
//
// The above code is commented out in favor of our fourth version of switch
// code block. This version demonstrates binding a value to the local variable,
// errorString, used in the switch block.

//var statusCode: Int = 404
//
//var errorString: String = "The request failed with the error: "
//
//switch statusCode   {
//    
//case 100, 101:
//    errorString += " Informational, \(statusCode)."
//    print(errorString)
//    
//case 204:
//    errorString += " Successful, but no content, \(statusCode)."
//    print(errorString)
//    
//case 300...307:
//    errorString += " Redirection, \(statusCode)."
//    print(errorString)
//    
//case 400...417:
//    errorString += " Client error, \(statusCode)."
//    print(errorString)
//    
//case 500...505:
//    errorString += " Server error, \(statusCode)."
//    print(errorString)
//    
//default:
//    errorString = "\(statusCode) is not a known error code."
//    print(errorString)
//    
//}   // end switch statusCode


// NOTE:
//
// The above code is commented out in favor of our fifth version of the
// switch block, which now features a where clause, allowing us to filter
// out error messages with a status code beginning in 200, because these
// are not in fact error messages, but success messages.

//var statusCode: Int = 204
//
//var errorString: String = "The request failed with the error: "
//
//switch statusCode   {
//    
//case 100, 101:
//    errorString += " Informational, \(statusCode)."
//    print(errorString)
//    
//case 204:
//    errorString += " Successful, but no content, 204."
//    print(errorString)
//    
//case 300...307:
//    errorString += " Redirection, \(statusCode)."
//    print(errorString)
//    
//case 400...417:
//    errorString += " Client error, \(statusCode)."
//    print(errorString)
//    
//case 500...505:
//    errorString += " Server error, \(statusCode)."
//    print(errorString)
//    
//case let unknownCode where (unknownCode >= 200 && unknownCode < 300)
//    || unknownCode > 505:
//    errorString = "\(unknownCode) is not a known error code."
//    print(errorString)
//    
//default:
//    errorString = "Unexpected error encountered."
//    print(errorString)
//    
//}   // end switch statusCode



// NOTE:
//
// The above code is commented out in favor of our sixth version of the
// switch code block. This time, we institute a tuple, which encapsulates
// the values found in two variables: statusCode: Int and errorString: String.

//var statusCode: Int = 418
//
//var errorString: String = "The request failed with the error: "
//
//switch statusCode   {
//    
//case 100, 101:
//    errorString += " Informational, \(statusCode)."
//    print(errorString)
//    
//case 204:
//    errorString += " Successful, but no content, 204."
//    print(errorString)
//    
//case 300...307:
//    errorString += " Redirection, \(statusCode)."
//    print(errorString)
//    
//case 400...417:
//    errorString += " Client error, \(statusCode)."
//    print(errorString)
//    
//case 500...505:
//    errorString += " Server error, \(statusCode)."
//    print(errorString)
//    
//case let unknownCode where (unknownCode >= 200 && unknownCode < 300)
//    || unknownCode > 505:
//    errorString = "\(unknownCode) is not a known error code."
//    print(errorString)
//    
//default:
//    errorString = "Unexpected error encountered."
//    print(errorString)
//    
//}   // end switch statusCode
//
//let error = (statusCode, errorString)
//
//// Access the tuple elements:
//
//// Put a line break after previous console output.
//
//print()
//print(error.0)
//print(error.1)


// NOTE:
//
// The above code is commented out in favor of our seventh version of
// the switch code block. This time, for easier accessibility and
// readability of code, we name the tuple elements and can reference
// them by name rather than index number.

var statusCode: Int = 418

var errorString: String = "The request failed with the error: "

switch statusCode   {
    
case 100, 101:
    errorString += " Informational, \(statusCode)."
    print(errorString)
    
case 204:
    errorString += " Successful, but no content, 204."
    print(errorString)
    
case 300...307:
    errorString += " Redirection, \(statusCode)."
    print(errorString)
    
case 400...417:
    errorString += " Client error, \(statusCode)."
    print(errorString)
    
case 500...505:
    errorString += " Server error, \(statusCode)."
    print(errorString)
    
case let unknownCode where (unknownCode >= 200 && unknownCode < 300)
    || unknownCode > 505:
    errorString = "\(unknownCode) is not a known error code."
    print(errorString)
    
default:
    errorString = "Unexpected error encountered."
    print(errorString)
    
}   // end switch statusCode

let error = (code: statusCode, error: errorString)

// Access the tuple elements:

// Put a line break after previous console output.

print()
print(error.code)
print(error.error)

// Perform pattern matching in switch block using tuples:

// First declare two constants of error codes.

let firstErrorCode = 404

let secondErrorCode = 200

// Now declare a tuple comprised of the two error codes.

let errorCodes = (firstErrorCode, secondErrorCode)

// switch code block that evaluates cases of errorCodes.

switch errorCodes   {
    
case (404, 404):
    print("No items found")
    
// NOTE: The underscore character, (_) denotes a wildcard.
    
case (404, _):
    print("\nFirst item not found.")
    
case (_, 404):
    print("\nSecond item not found.")
    
default:
    print("\nAll items found.")
    
}   // end switch errorCodes

// An awkward single switch statement:

//let age = 25
//
//switch age {
//    
//case 18...35:
//    print("\nCool demographic!")
//    
//default:
//    break
//    
//}   // end switch age


// NOTE:
//
// The above code is commented out in favor of an alternative to
// the single case switch block.

// Here we accomplish the same think as the switch block above,
// using the if case statement.

let age = 25

if case 18...35 = age {
    
    print("\nCool demographic!")
    
}   // end if case

// An if case statement with a where clause:

if case 18...35 = age where age >= 21   {
    
    print("\nIn cool demographic and of drinking age!")
    
}   // end if case where age >= 21
