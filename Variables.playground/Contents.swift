//: Playground - Variables.

// Explore data types.

import Cocoa

// Initialize String for number of stoplights.

//var numberOfStopLights = "Four"

// Add an Integer to the above String type.
// NOTE: This will throw an error.

//numberOfStopLights += 2

// NOTE: Code above is commented out so that it may be refactored
// correctly. In the code that follows, no attempt is made to add
// an Int to a String, as numberOfStopLights is converted to an
// Integer.

//var numberOfStopLights: Int = 4
//
//numberOfStopLights += 2

// NOTE: Code above is commented out so that we may explore the nature
// of variables and constants, and the differences between them. The code
// below will definitely throw an error as you cannot change the value
// of a constant.

let numberOfStopLights: Int = 4

//numberOfStopLights += 2

// NOTE: Last line of code commented out to resolve error of attempting to
// change the value of a constant.

// Add a variable for population.

var population: Int

// Set the value of population.

population = 5422

// Set constant for town name.

let townName: String = "Knowhere"

// Using String interpolation to create a town description:

let townDescription =
"\(townName) has a population of \(population) and" +
" \(numberOfStopLights) stoplights.\n"

// Log townDescription to console.

print(townDescription)


// BRONZE CHALLENGE:
//
// Add a new variable to your playground representing Knowhere’s level
// of unemployment. Which data type should you use? Give this variable
// a value and update townDescription to use this new information.

// MY SOLUTION:

var unemploymentRate: Double = 13.27

let newTownDescription =
"\n\(townDescription)The current rate of unemployment is \(unemploymentRate)%.\n"

// Log newTownDescription to console.

print(newTownDescription)